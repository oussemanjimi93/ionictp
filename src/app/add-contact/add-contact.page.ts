import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.page.html',
  styleUrls: ['./add-contact.page.scss'],
})
export class AddContactPage implements OnInit {
  addForm: FormGroup
  params: any
  constructor(private modalController: ModalController, private formBuilder: FormBuilder,
    public alertController: AlertController,
    private navParams: NavParams) { }

  ngOnInit() {
    this.params = this.navParams.get('contacts');
    console.log("params", this.params)
    this.initForm()
  }
  initForm() {
    this.addForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      tel1: ['', Validators.required],
      tel2: ['', Validators.required],
      type: ['', Validators.required],

    })
  }
  dismiss(data) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'Array': data,
      'is':true
    });
  }
  dismiss2() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss();
  }
  addContact() {
    var exsit = false
    if (this.addForm.valid) {
      console.log(this.addForm.value)
      for (let item of this.params) {
        console.log("item", item.tel1)
        if (item.tel1 == this.addForm.get("tel1").value) {

          exsit = true
          this.presentAlert("contact exsit déja")
          break;
        }

      }
      if (!exsit) {
        this.presentAlert("contact ajouter avec success")
        this.params.push(this.addForm.value)
        console.log("new array",this.params)
        this.dismiss(this.params)
      }
    } else {
      this.presentAlert("Voulez vous remplire toutes le formulaire")
    }
  }
  async presentAlert(msg) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Information',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
  
}
