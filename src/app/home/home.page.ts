import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddContactPage } from '../add-contact/add-contact.page';
import { ContactInfoPage } from '../contact-info/contact-info.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  segementStateModel = "amis"
  contact = [{
    'nom': 'oussema',
    'prenom': 'Njimi',
    'tel1': "27898321",
    'tel2': "27898321",
    "date_creation": '25/11/2021',
    "maj": '25/11/2021',
    "type": "amis"
  },
  {
    'nom': 'sami',
    'prenom': 'Khlifi',
    'tel1': "21321232",
    'tel2': "21321232",
    "date_creation": '25/11/2021',
    "maj": '25/11/2021',
    "type": "amis"
  },
  {
    'nom': 'asma',
    'prenom': 'asma',
    'tel1': "22222222",
    'tel2': "33333333",
    "date_creation": '25/11/2021',
    "maj": '25/11/2021',
    "type": "famille"
  },
  {
    'nom': 'amani',
    'prenom': 'tests',
    'tel1': "32987893",
    'tel2': "32987893",
    "date_creation": '25/11/2021',
    "maj": '25/11/2021',
    "type": "famille"
  },
  {
    'nom': 'yosri',
    'prenom': 'yosri',
    'tel1': "21238798",
    'tel2': "21238798",
    "date_creation": '25/11/2021',
    "maj": '25/11/2021',
    "type": "professionnel"
  },
  {
    'nom': 'raw3a',
    'prenom': 'raw3a',
    'tel1': "32187987",
    'tel2': "32187987",
    "date_creation": '25/11/2021',
    "maj": '25/11/2021',
    "type": "autre"
  },
  ]
  constructor(private route: Router, public modalController: ModalController) { }
  segmentChanged(event) {
    console.log("event segement", event.detail.value)
    this.segementStateModel = event.detail.value
  }

  async addContact() {
    console.log("handle go to add")
    const modal = await this.modalController.create({
      component: AddContactPage,
      cssClass: 'my-custom-class',
      mode: "ios",
      componentProps: {
        contacts: this.contact
      }
    });
    modal.onDidDismiss().then((res:any)=>{
      console.log("close",res.data.Array)
      console.log("old",this.contact)
      if(res.data.is){
         this.contact = res.data.Array
      }
     
    });
    return await modal.present();

  }

  async contactInfo(item) {
    console.log("item", item)
    const modal = await this.modalController.create({
      component: ContactInfoPage,
      cssClass: 'my-custom-class',
      mode: "ios",
      componentProps: {
        items: item
      }
    });
    return await modal.present();
  }

}
